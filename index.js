/*1. Create an activity.js file on where to write and save the solution for the activity.
2. Find users with letter s in their first name or d in their last name.
Use the $or operator.
Show only the firstName and lastName fields and hide the _id field.
3. Find users who are from the HR department and their age is greater than or equal to 70.
Use the $and operator
4. Find users with the letter e in their first name and has an age of less than or equal to 30.
Use the $and, $regex and $lte operators
5. Create a git repository named s29.
6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
7. Add the link in Boodle.*/




	db.users.insert({
	    "_id" : ObjectId("63ecacca208209a095a7a576"),
	    "firstName" : "Jane",
	    "lastName" : "Doe",
	    "age" : 21.0,
	    "contact" : {
	        "phone" : "987653421",
	        "email" : "janedoe@mail.com"
	    },
	    "courses" : [ 
	        "CSS", 
	        "Javascript", 
	        "Phyton"
	    ],
	    "department" : "HR"
	});

/* 2 */
db.users.insert({
    "_id" : ObjectId("63ecafa7208209a095a7a577"),
    "firstName" : "Stephen",
    "lastName" : "Hawking",
    "age" : 76.0,
    "contact" : {
        "phone" : "98756412",
        "email" : "theoryofeverithing@mail.com"
    },
    "courses" : [ 
        "Phyton", 
        "React", 
        "PHP"
    ],
    "department" : "HR"
});

/* 3 */
db.users.insert({
    "_id" : ObjectId("63ecafa7208209a095a7a578"),
    "firstName" : "Neil",
    "lastName" : "Armstrong",
    "age" : 82.0,
    "contact" : {
        "phone" : "985464757",
        "email" : "neiltothemoon@mail.com"
    },
    "courses" : [ 
        "React", 
        "Laravel", 
        "Sass"
    ],
    "department" : "HR"
});

/* 4 */
db.users.insert({
    "_id" : ObjectId("63ecb5561800c60945649b9a"),
    "firstName" : "Bill",
    "lastName" : "Gates",
    "age" : 65.0,
    "contact" : {
        "phone" : "1234567890",
        "email" : "billgates@mail.com"
    },
    "courses" : [ 
        "PHP", 
        "Laravel", 
        "HTML"	
    ],
    "department" : "Operations",
    "status" : "active"
});



db.users.find({ $or: [{ firstName: { $regex: 's', $options: '$i'} }, { lastName: { $regex: 'd', $options: '$i'} }] }, { firstName: 1, lastName: 1, _id: 0});

db.users.find({ $and: [{ age: {$gte: 70} },{department: "HR"}] });

db.users.find({ $and: [{ age: {$lte: 30} },{ firstName: { $regex: 'e', $options: '$i'} }] });




